import Profile from "../../../assets/icons/profile.svg"
import Skills from "../../../assets/icons/skill.svg"
import Works from "../../../assets/icons/works.svg"
import Contacts from "../../../assets/icons/contacts.svg"
import classNames from "classnames";

export const links = [
    {
        name: 'Profile',
        icon: <Profile className={classNames("navigation-panel__link-icon")}/>
    },
    {
        name: 'Skills',
        icon: <Skills className={classNames("navigation-panel__link-icon")}/>
    },
    {
        name: 'Works',
        icon: <Works className={classNames("navigation-panel__link-icon")}/>
    },
    {
        name: 'Contacts',
        icon: <Contacts className={classNames("navigation-panel__link-icon")}/>
    },
]
