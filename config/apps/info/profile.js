import React from "react";
import images from "../../images"
import FullStack from "/assets/icons/fullstack.svg"
import Design from "/assets/icons/design.svg"
import Gold from "/assets/icons/gold-player.svg"
import Sad from "/assets/icons/sad.svg"

export const photos = [
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
    images.alexxx1,
]

export const bio = [
    {
        icon: <FullStack/>,
        headerDark: "Full stack web",
        header: "developer",
    },
    {
        icon: <Design/>,
        headerDark: "Mediocre",
        header: "designer",
    },
    {
        icon: <Gold/>,
        headerDark: "Gold 3",
        header: "player",
    },
    {
        icon: <Sad/>,
        headerDark: "And just not ",
        header: "really",
    },
]

export const bioText = "I am trying to develop at least in this area, and how successful this event is, " +
    "you are given the opportunity to evaluate for yourself, by reviewing the completed work. HF <3" +
    "\nAfter a happy " +
    "and extremely senseless waste of 4 years of his life on higher education and moral decay, he began " +
    "to engage in web development."
