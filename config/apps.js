import Chat from "../assets/icons/apps/chat.svg"
import Gallery from "../assets/icons/apps/gallery.svg"
import InfoIcon from "../assets/icons/apps/user.svg"
import CatIcon from "../assets/icons/apps/cat.svg"
import React from "react";
import Loader from "../components/Loader";
import Info from "../components/apps/info/Info";
import classNames from "classnames";
import dynamic from "next/dynamic";
import windowThemes from "./constants/windowThemes";
import Cat from "../components/apps/cat/Cat";

const apps = [
    {
        theme: windowThemes.LIGHT,
        icon:  <Gallery />,
        window: <span>app</span>,
        name:  "Gallery",
        slug: "gallery_app",
        description:  "My works",
        top: 100,
        left: 100,
        minWidth: 715,
        minHeight: 584,
        resize: true,
    },
    {
        theme: windowThemes.LIGHT,
        icon:  <InfoIcon />,
        window: <Info />,
        name:  "Info",
        slug: "info_app",
        description:  "About my and my life",
        top: 100,
        left: 100,
        minWidth: 1200,
        minHeight: 746,
        resize: true,
    },
    {
        theme: windowThemes.LIGHT,
        icon:  <Chat />,
        window: <span>app</span>,
        name:  "Contacts",
        slug: "contacts_app",
        description:  "Chat my anywhere",
        top: 100,
        left: 100,
        minWidth: 715,
        minHeight: 584,
        resize: true,
    },
    {
        theme: windowThemes.DARK,
        icon:  <CatIcon />,
        window: <Cat />,
        name:  "Cat.jsx",
        slug: "cat_app",
        description:  "Just rolling cat",
        top: 100,
        left: 100,
        minWidth: 400,
        minHeight: 450,
        resize: false,
    },
]

export default apps;
