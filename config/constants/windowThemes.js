const windowThemes = {
    DARK: 'dark',
    LIGHT: 'light',
}

export default windowThemes;
