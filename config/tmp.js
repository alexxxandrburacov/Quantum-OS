import Chat from "../assets/icons/apps/chat.svg"
import React from "react";

export const notifications = [
    {
        icon: <Chat />,
        header: "Saint INC",
        text: "Why did you write to me? Did I do something to you? What for?"
    },
];
