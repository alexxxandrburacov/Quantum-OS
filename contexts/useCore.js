import {useContext} from "react";
import CoreContext from "./context";

const useCore = () => {
    return useContext(CoreContext);
}

export default useCore;
