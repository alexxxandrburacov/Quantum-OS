import React, {useState} from 'react';
import apps from "../../config/apps";
import {notifications} from "../../config/tmp";
import CoreContext from "../../contexts/context";
import animations from "../../animations";

const Core = ({children}) => {
    const [appsOpen, setAppsOpen] = useState(false)
    const [notificationsOpen, setNotificationsOpen] = useState(false)
    const [runningApps, setRunningApps] = useState([])

    const openWindow = (appName, windowProps = {}) => {
        const app = apps.find(app => app.name === appName);
        let runningApp = {}

        if (app && !runningApps.find(app => app.name === appName)) {
            let top = 100 + 20 * runningApps.length
            let left = 100 + 20 * runningApps.length

            if (top + app.minHeight > window.innerHeight - 80) {
                top = 12
            }
            if (left + app.minWidth > window.innerWidth - 12) {
                left = 12
            }

            runningApp = {
                ...app,
                transform: {
                    top: top,
                    left: left,
                    width: app.minWidth,
                    height: app.minHeight,
                },
                app: app.window,
                minimized: false,
            }
            setRunningApps([...runningApps, runningApp]);
            return true;
        } else {
            return false
        }
    }

    const minimizeWindow = (slug, value) => {
        const appWindow = document.querySelector(`.${slug.toLowerCase()}-app`)

        animations.minimizeWindow(
            appWindow,
            value,
            () => setRunningApps(runningApps => {
                const newRunningApps = [...runningApps]
                newRunningApps[newRunningApps.findIndex(app => app.slug === slug)]["minimized"] = value
                return newRunningApps
            })
        )
    }

    const closeWindow = (slug) => {
        animations.closeWindowAnim(
            `.${slug.toLowerCase()}-app`,
            () => {
                setRunningApps(runningApps.filter(app => app.slug !== slug))
            }
        )

        return true;
    }

    const windowFocus = (appName) => {
        setRunningApps(array => {
            const maxZIndex = array.reduce((acc, app) => {
                if (app.zIndex > acc) {
                    acc = app.zIndex;
                }
                console.log({...app})
                if (!('zIndex' in app) && acc < 11) {
                    acc = 10;
                }
                return acc;
            }, 0);
            return array.map(app => {
                if (app.name === appName) {
                    app.zIndex = maxZIndex + 1;
                }
                return app;
            })
        })
    }

    const contextValue = {
        isAppsOpen: appsOpen,
        setAppsOpen: setAppsOpen,
        isNotificationsOpen: notificationsOpen,
        runningApps,
        setRunningApps,
        notifications, // TODO
        apps,
        openWindow,
        closeWindow,
        windowFocus,
        setNotificationsOpen,
        minimizeWindow,
    }

    return (
        <CoreContext.Provider value={contextValue}>
            {children}
        </CoreContext.Provider>
    )
}

export default Core;
