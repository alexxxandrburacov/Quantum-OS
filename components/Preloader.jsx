import React, {useEffect, useRef, useState} from 'react';
import classNames from "classnames";

function Preloader({className, photo, photoClass}) {
    const ref = useRef(null)
    const [loaded, setLoaded] = useState(false)
    const onLoad = () => {
        setLoaded(true)
    }

    return (
        <div className={classNames("preloader", className)}>
            <img
                onLoad={onLoad}
                ref={ref}
                src={photo.src}
                alt=""
                className={classNames(
                    "preloader__image",
                    "preloader__image--photo",
                    photoClass,
                    {"preloader__image--hidden": !loaded}
                )}
            />
            <div className={classNames("preloader__image-blur")}>
                <img
                    src={photo.preloader}
                    alt=""
                    className={classNames(
                        "preloader__image",
                        "preloader__image--loader",
                        photoClass,
                        {"preloader__image--hidden": loaded}
                    )}
                />
            </div>

        </div>

    );
}

export default Preloader;
