import React from 'react';
import classNames from "classnames";
import ContentContainer from "../../layout/ContentContainer";

function IconCard({ className, content, index, onClick = () => {} }) {
    const shapes = [
        "icon-card__icon-container--ellipse",
        "icon-card__icon-container--rectangle",
        "icon-card__icon-container--rhombus"
    ]
    if (index >= shapes.length) {
        index = 0
    }
    return (
        <ContentContainer onClick={onClick} className={classNames("icon-card", className)} isSmall>

            <div className={classNames("icon-card__icon-container", shapes[index])}>
                {React.cloneElement(content.icon, {
                    className: "icon-card__icon"
                })}
            </div>

            <div className={classNames("icon-card__text-container")}>
                <span className={classNames("icon-card__header", "icon-card__header--dark")}>
                {content.headerDark}
            </span>

                <span className={classNames("icon-card__header")}>
                {content.header}
            </span>
            </div>

        </ContentContainer>
    );
}

export default IconCard;
