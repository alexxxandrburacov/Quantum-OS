import React from 'react';
import classNames from "classnames";
import RunningLine from "../RunningLine";

function SkillCard({ className, content = [] }) {
    content = {
        header: "React",
        description: "I can make a website, simple or more complex, for example, like this. " +
            "As far as my experience goes, I try to keep the code clean and convenient, readable architecture."
    }
    return (
        <div className={classNames("skill-card", className)}>
            <h3 className={classNames("skill-card__header")}>{content.header}</h3>
            <RunningLine className={classNames("skill-card__running-line")} width={13} height={5}/>
            <p className={classNames("skill-card__description")}>{content.description}</p>
        </div>
    );
}

export default SkillCard;
