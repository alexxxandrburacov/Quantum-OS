import React from 'react';
import classNames from "classnames";
import ContentContainer from "../../layout/ContentContainer";

function TextCard({className, text}) {
    return (
        <ContentContainer className={classNames("text-card", className)} isSmall>
            <span className={classNames("text-card__text")}>
                {text}
            </span>
        </ContentContainer>
    );
}

export default TextCard;
