import React from 'react';
import classNames from "classnames";

const Cat = ({ className }) => {
    return (
        <div className={classNames(className, 'cat')} />
    )
}

export default Cat;
