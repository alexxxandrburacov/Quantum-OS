import React from 'react';
import classNames from "classnames";
import { avatar } from "../../../config/apps/info/avatar";
import Preloader from "../../Preloader";

function Avatar({ className }) {
    return (
        <div className={classNames("avatar", className)}>
            <Preloader photo={avatar.photo} photoClass="avatar__photo" className={classNames("avatar__preloader")}/>
            <span className={classNames("avatar__name")}>{avatar.name}</span>
        </div>
    );
}

export default Avatar;
