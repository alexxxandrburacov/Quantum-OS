import React, {useState} from 'react';
import classNames from "classnames";
import { links } from "../../../config/apps/info/links";

function NavigationPanel({ className, setPage }) {
    const [activeLink, setActiveLink] = useState(0)
    const activateLink = index => {
        setActiveLink(index)
    }

    return (
        <div className={classNames("navigation-panel", className)}>
            {links.map( (link, index) =>
                <div key={index} className={classNames(
                    "navigation-panel__link",
                    {"navigation-panel__link--active": index === activeLink}
                )}
                     onClick={() => {
                         activateLink(index)
                         setPage(link.name)
                     }}
                >
                    <div className={classNames("navigation-panel__active-mark")}/>
                    {link.icon}
                    <span className={classNames("navigation-panel__link-name")}>{link.name}</span>
                </div>
            )}
        </div>
    );
}

export default NavigationPanel;
