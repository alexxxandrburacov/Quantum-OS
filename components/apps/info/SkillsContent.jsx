import React from 'react';
import ContentContainer from "../../../layout/ContentContainer";
import classNames from "classnames";
import SkillCard from "../../cards/SkillCard";

function SkillsContent({className}) {
    return (
        <ContentContainer className={classNames("skills-content", className)}>

            <h2 className={classNames("skills-content__name")}>Skills</h2>

            <div className={classNames("skills-content__columns-container")}>
                <div className={classNames("skills-content__content-column")}>
                    <SkillCard className={classNames("skills-content__skill-card")}/>
                    <SkillCard className={classNames("skills-content__skill-card")}/>
                </div>

                <div className={classNames("skills-content__content-column")}>
                    <SkillCard className={classNames("skills-content__skill-card")}/>
                </div>

            </div>
        </ContentContainer>
    );
}

export default SkillsContent;
