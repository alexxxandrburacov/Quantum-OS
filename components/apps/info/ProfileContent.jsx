import React from 'react';
import ContentContainer from "../../../layout/ContentContainer";
import classNames from "classnames";
import {photos} from "../../../config/apps/info/profile";
import Preloader from "../../Preloader";

function ProfileContent({className}) {
    return (
        <ContentContainer className={classNames("profile-content", className)}>

            <h2 className={classNames("profile-content__name")}>Ihor Babin</h2>

            <div className={classNames("profile-content__columns-container")}>
                <div className={classNames("profile-content__content-column")}>
                    {photos.map((photo, index) => {
                            if (index < photos.length / 2) {
                                return <Preloader
                                    key={index}
                                    className={classNames("profile-content__preloader")}
                                    photoClass={classNames("profile-content__photo")}
                                    photo={photo}
                                />
                            }
                        }
                    )}
                </div>

                <div className={classNames("profile-content__content-column")}>
                    {photos.map((photo, index) => {
                            if (index >= photos.length / 2) {
                                return <Preloader
                                    key={index}
                                    className={classNames("profile-content__preloader")}
                                    photoClass={classNames("profile-content__photo")}
                                    photo={photo}
                                />
                            }
                        }
                    )}
                </div>

            </div>
        </ContentContainer>
    );
}

export default ProfileContent;
