import React, {useState} from 'react';
import classNames from "classnames";
import ContentContainer from "../../../layout/ContentContainer";
import NavigationPanel from "./NavigationPanel";
import Avatar from "./Avatar";
import ProfileContent from "./ProfileContent";
import SkillsContent from "./SkillsContent";
import {bio, bioText} from "../../../config/apps/info/profile";
import IconCard from "../../cards/IconCard";
import TextCard from "../../cards/TextCard";

function Info({className}) {
    const [activePage, setActivePage] = useState("Profile")
    return (
        <div className={classNames("info", className)}>
            <ContentContainer className={classNames("info__category-panel")} isSmall>
                <Avatar className={classNames("info__avatar")}/>
                <NavigationPanel className={classNames("info__navigation")} setPage={setActivePage}/>
            </ContentContainer>

            {activePage === "Profile" && <ProfileContent className={classNames("info__content")}/>}
            {activePage === "Skills" && <SkillsContent className={classNames("info__content")}/>}
            {activePage === "Works" && <span>Works</span>}
            {activePage === "Contacts" && <span>Contacts</span>}

            <div className={classNames("info__text-blocks-container")}>
                <div className={classNames("info__skills-container")}>
                    {bio.map((block, index) =>
                        <IconCard
                            className={classNames("info__skill-block")}
                            content={block}
                            index={index}
                            key={index}
                        />
                    )}
                </div>
                <TextCard className={classNames("info__text-block")} text={bioText}/>
            </div>
        </div>
    );
}

export default Info;
