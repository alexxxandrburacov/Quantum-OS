import React, {useEffect, useRef, useState} from 'react';
import classNames from "classnames";
import Background from "../../layout/Background";
import Notification from "../Notification";
import AppWindow from "../appWindow/AppWindow";
import AppsList from "../AppsList";
import TaskBar from "../TaskBar";
import useCore from "../../contexts/useCore";

const Main = () => {
    const {
        notifications,
        isNotificationsOpen,
        runningApps,
        apps,
        isAppsOpen,
        setAppsOpen,
    } = useCore();

    const appsListRef = useRef(null)
    const notificationRef = useRef(null)

    return (
        <div className={classNames("home")}>
            <div className="home__container">
                <Background/>
                <Notification
                    ref={notificationRef}
                    className={classNames("home__notification")}
                    content={notifications[0]}
                    isOpen={isNotificationsOpen}
                />
                {runningApps.map(app =>
                    <AppWindow
                        key={app.name}
                        className={classNames("home__app", app.slug.toLowerCase() + "-app")}
                        content={app}
                    />
                )
                }
                <AppsList
                    ref={appsListRef}
                    className={classNames("home__apps-list")}
                    content={apps}
                    isOpen={isAppsOpen}
                    setOpen={setAppsOpen}
                />
                <TaskBar
                    className={classNames("home__task-bar")}
                    appsListRef={appsListRef}
                    notificationRef={notificationRef}
                />
            </div>
        </div>
    )
}

export default Main;
