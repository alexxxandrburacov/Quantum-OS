import useCore from "../../contexts/useCore";
import {useEffect, useState} from "react";
import {em} from "../../tools/helpers";
import actionTypes from "./actionTypes";

const useWindow = (content) => {
    const {windowFocus} = useCore();
    const [isFullScreen, setFullScreen] = useState(false);
    const [action, setAction] = useState(false)

    useEffect(() => {
        const appWindow = document.querySelector(`.${content.slug.toLowerCase()}-app`);

        if (isFullScreen) {
            appWindow.style.width = null;
            appWindow.style.height = null;
            appWindow.style.top = 12 + 'px';
            appWindow.style.left = 12 + 'px';
            // setFullScreen(true)
        } else {
            appWindow.style.width = content.transform.width + 'px';
            appWindow.style.height = content.transform.height + 'px';

            if (!action) {
                appWindow.style.top = content.transform.top + 'px';
                appWindow.style.left = content.transform.left + 'px';
            } else {
                content.transform.top = 12;
                content.transform.left = 12;
            }
            // setFullScreen(false)
        }
    }, [isFullScreen])

    useEffect(() => {
        const appWindow = document.querySelector(`.${content.slug.toLowerCase()}-app`);
        const dragBar = document.querySelector(`.drag-bar__${content.slug.toLowerCase()}`);
        const rightTarget = document.querySelector(`.app-window-${content.slug}__border--right`);
        const cornerTarget = document.querySelector(`.app-window-${content.slug}__border--corner`);
        const bottomTarget = document.querySelector(`.app-window-${content.slug}__border--bottom`);

        const defaultPadding = 12;
        const bottomPadding = 80;

        let transformX = content.left;
        let transformY = content.top;

        let resizeWidth = null;
        let resizeHeight = null;

        let offsetX = null;
        let offsetY = null;

        let actionType = null;

        windowFocus(content.name);

        const getResizeType = target => {
            let resizeType = null;

            switch (target) {
                case cornerTarget: {
                    resizeType = actionTypes.RESIZE_XY;
                    break;
                }
                case rightTarget: {
                    resizeType = actionTypes.RESIZE_X;
                    break;
                }
                case bottomTarget: {
                    resizeType = actionTypes.RESIZE_Y;
                    break;
                }
            }
            return resizeType;
        }

        const validateAndComputePosition = (
            transform,
            site,
            windowSite,
            minPadding,
            maxPadding,
        ) => {
            let validatedTransform;

            switch (true) {
                case (transform + site > windowSite - maxPadding): {
                    validatedTransform = windowSite - maxPadding - site;
                    break;
                }
                case (transform < minPadding): {
                    validatedTransform = minPadding;
                    break;
                }
                default: {
                    validatedTransform = transform;
                }
            }

            return validatedTransform;
        }

        const dragWindow = event => {
            const windowRect = appWindow.getBoundingClientRect();

            transformX = validateAndComputePosition(
                event.clientX - offsetX,
                windowRect.width,
                window.innerWidth,
                defaultPadding,
                defaultPadding,
            );
            transformY = validateAndComputePosition(
                event.clientY - offsetY,
                windowRect.height,
                window.innerHeight,
                defaultPadding,
                bottomPadding,
            );

            appWindow.style.transform = `translate(${transformX - content.transform.left}px, ${transformY - content.transform.top}px)`
        }

        const validateAndComputeSite = (
            resizeSite,
            windowSite,
            minSite,
            padding,
            position,
            property,
        ) => {
            switch (true) {
                case (
                    resizeSite > minSite
                    && resizeSite + position < windowSite - padding
                ): {
                    appWindow.style[property] = (content.transform[property] = resizeSite) + 'px';
                    break;
                }
                case (resizeSite < minSite): {
                    appWindow.style[property] = (content.transform[property] = minSite) + 'px';
                    break;
                }
                case (resizeSite + padding > windowSite - padding): {
                    appWindow.style[property] = (content.transform[property] = windowSite - padding - position) + 'px';
                    break;
                }
            }
        }

        const resizeWindow = event => {
            const windowRect = appWindow.getBoundingClientRect()

            // drop fullscreen on resize
            // setFullScreen(false);

            // формируем новую высоту и добаляем оффсет для иделаьной работы
            resizeWidth = event.clientX - windowRect.left + offsetX;
            resizeHeight = event.clientY - windowRect.top + offsetY;

            switch (actionType) {
                case actionTypes.RESIZE_Y: {
                    resizeWidth = null
                    break;
                }
                case actionTypes.RESIZE_X: {
                    resizeHeight = null
                    break;
                }
            }

            // валидурем и записываем новые значения ресайза
            validateAndComputeSite(
                resizeWidth,
                window.innerWidth,
                content.minWidth,
                defaultPadding,
                windowRect.left,
                'width',
            );

            validateAndComputeSite(
                resizeHeight,
                window.innerHeight,
                content.minHeight,
                bottomPadding,
                windowRect.top,
                'height',
            );
        }

        const mouseUp = () => {
            switch (actionType) {
                case actionTypes.MOVE: {
                    appWindow.style.transform = "translate(0, 0)";

                    appWindow.style.left = (content.transform.left = transformX) + 'px';
                    appWindow.style.top = (content.transform.top = transformY) + 'px';

                    break;
                }
            }

            if (actionType) {
                actionType = null;
                setAction(false);
                window.document.body.style.cursor = '';
            }
        }

        const mouseMove = event => {
            if (actionType === actionTypes.MOVE) {
                dragWindow(event);
            }

            if ([actionTypes.RESIZE_Y, actionTypes.RESIZE_X, actionTypes.RESIZE_XY].includes(actionType)) {
                resizeWindow(event);
            }
        }

        const mouseDown = event => {
            const resizeType = getResizeType(event.target);

            offsetX = event.offsetX;
            offsetY = event.offsetY;

            setFullScreen(isFullScreen => {
                let fullScreen = isFullScreen;

                switch (true) {
                    case (event.target === dragBar): {
                        actionType = actionTypes.MOVE;
                        setAction(true);
                        window.document.body.style.cursor = resizeType;
                        fullScreen = false;
                        break;
                    }
                    case (resizeType && content.resize && !isFullScreen): {
                        actionType = resizeType;
                        setAction(true);
                        window.document.body.style.cursor = resizeType;
                        break;
                    }
                }

                return fullScreen;
            })

            windowFocus(content.name);
        }

        window.addEventListener('mouseup', mouseUp)
        appWindow?.addEventListener('mousedown', mouseDown)
        window.addEventListener('mousemove', mouseMove)

        return () => {
            window.removeEventListener('mouseup', mouseUp)
            appWindow?.removeEventListener('mousedown', mouseDown)
            window.removeEventListener('mousemove', mouseMove)
        }
    }, [])

    return {
        isFullScreen,
        setFullScreen,
    }
}

export default useWindow;
