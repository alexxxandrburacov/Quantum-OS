const actionTypes = {
    RESIZE_X: 'w-resize',
    RESIZE_Y: 'n-resize',
    RESIZE_XY: 'nw-resize',
    MOVE: 'grab',
}

export default actionTypes;
