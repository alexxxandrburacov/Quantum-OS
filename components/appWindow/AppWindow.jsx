import React from 'react';
import classNames from "classnames";
import DragBar from "../../ui/DragBar";
import useWindow from "./useWindow";
import {em} from "../../tools/helpers";
import windowThemes from "../../config/constants/windowThemes";


function AppWindow({className, content}) {
    const {
        isFullScreen,
        setFullScreen,
    } = useWindow(content);

    return (
        <div
            className={classNames(
                "app-window",
                `app-window--${content.theme || windowThemes.LIGHT}`,
                className,
                {
                    "app-window--full-screen": isFullScreen,
                }
            )}
            style={{
                top: em(content.transform.top),
                left: em(content.transform.left),
                width: em(content.transform.width),
                height: em(content.transform.height),
                zIndex: content.zIndex,
            }}>
            <DragBar
                className={classNames(
                    "app-window__drag-bar",
                    "drag-bar__" + content.slug.toLowerCase()
                )}
                app={content}
                isFullScreen={isFullScreen}
                setFullScreen={setFullScreen}
            />
            <div
                className={classNames(
                    "app-window__app-container",
                    "app-container__" + content.slug.toLowerCase()
                )}
            >
                {React.cloneElement(content.app, {
                    className: "app-window__app",
                })}

                <div className={classNames(
                    "app-window__border",
                    "app-window__border--right",
                    `app-window-${content.slug}__border--right`
                )}/>
                <div className={classNames(
                    "app-window__border",
                    "app-window__border--corner",
                    `app-window-${content.slug}__border--corner`
                )}/>
                <div className={classNames(
                    "app-window__border",
                    "app-window__border--bottom",
                    `app-window-${content.slug}__border--bottom`
                )}/>
            </div>
        </div>
    );
}

export default AppWindow;
