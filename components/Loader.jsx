import React from 'react';
import classNames from "classnames";

const Loader = ({ className }) => {
    return (
        <div className={classNames(className, 'loader')}>
            <div className="loader__icon" />
        </div>
    )
}

export default Loader;
