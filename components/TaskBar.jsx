import React, { useContext } from 'react';
import AppsIcons from "../ui/AppsIcons";
import classNames from "classnames";
import TaskBarTools from "../ui/TaskBarTools";
import CoreContext from "../contexts/context"
import RunningApps from "./runningApps/RunningApps";
import animations from "../animations";


function TaskBar({className, appsListRef, notificationRef}) {
    const {setAppsOpen, isAppsOpen} = useContext(CoreContext)

    return (
        <div className={classNames(
            "task-bar",
            className
        )}>
            <AppsIcons
                onClick={() => {
                    animations.appsListAnim(appsListRef.current, isAppsOpen, setAppsOpen(!isAppsOpen))
                }}
                className={classNames(
                    "task-bar__apps-button",
                )}
                isAppsOpen={isAppsOpen}
            />

            <RunningApps className={classNames("task-bar__running-apps")}/>
            <TaskBarTools
                className={classNames("task-bar__tools")}
                notificationRef={notificationRef}
            />
        </div>
    );
}

export default TaskBar;
