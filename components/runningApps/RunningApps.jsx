import React, {useContext, useEffect, useState} from 'react';
import classNames from "classnames";
import CoreContext from "../../contexts/context"
import animations from "../../animations";
import {getTranslate} from "../../tools/helpers";

let app = null

function RunningApps({className}) {
    const { runningApps, windowFocus, minimizeWindow } = useContext(CoreContext);
    const [icons, setIcons] = useState([]);

    const getApp = (appName) => {
        return runningApps.find(element => element.name === appName)
    }

    useEffect(() => {
        if (runningApps.length < icons.length) {
            app = icons.find(app => {
                return !runningApps.find(element => element.name === app.name);
            })
            animations.appIconAnim(`.${app.slug.toLowerCase()}-icon`, true, () => {
                animations.runningAppsAnim(runningApps)
                setIcons(list => list.filter(icon => icon.name !== app?.name))
                app = null
            })
        }
        if (runningApps.length > icons.length) {

            setIcons(list => {
                app = runningApps.find(app => {
                    return !list.find(element => element.name === app.name);
                })

                return [
                    ...list,
                    {
                        name: app.name,
                        slug: app.slug,
                        icon: app.icon,
                    },
                ]
            } )
        }


    }, [runningApps])

    useEffect(() => {
        if (icons.length === 0 || runningApps.length < icons.length || !app) return

        const icon = document.querySelector(`.${app.slug.toLowerCase()}-icon`)

        if (icons.length !== 1) {
            icon.style.transform = "translateX(" +
                getTranslate(runningApps.length, (icons.length - 1) + 0.5) + "px)"
        }

        animations.appIconAnim(`.${app.slug.toLowerCase()}-icon`, false)
        animations.runningAppsAnim(runningApps)
    }, [icons])

    return (
        <div className={classNames("running-apps", className)}>
            {icons.map( app =>
                <div className={classNames("running-apps__app", app.slug.toLowerCase() + "-icon")} key={app.name}>
                    <div
                        className={classNames(
                            "running-apps__icon-container",
                            {
                                "running-apps__icon-container--minimized": getApp(app.name)?.minimized
                            }
                        )}
                        onClick={() => {
                            minimizeWindow(app.slug, !getApp(app.name)?.minimized)
                            windowFocus(app.name)
                        }}
                    >
                        {app.icon}
                    </div>
                </div>
            )}
        </div>
    );
}

export default RunningApps;
