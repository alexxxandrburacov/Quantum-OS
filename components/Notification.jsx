import React from 'react';
import classNames from "classnames";
import Rhombus from "../assets/icons/rhombus.svg"

const Notification = React.forwardRef(({ className, content, isOpen }, ref) => (
    <div ref={ref} className={classNames(
        "notification",
        className,
    )}>
        <div className={classNames("notification__icon")}>
            {content.icon}
        </div>
        <div className={classNames("notification__content")}>
            <span className={classNames("notification__header")}>{content.header}</span>
            <span className={classNames("notification__text")}>{content.text}</span>
        </div>
        <Rhombus className={classNames("notification__close-button")}/>
    </div>
))

export default Notification;
