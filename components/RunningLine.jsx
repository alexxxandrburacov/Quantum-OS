import React from 'react';
import classNames from "classnames";

function RunningLine({className, width, height}) {
    let arrayActive = [
        0b00000,
        0b11111,
        0b10100,
        0b01011,
        0b00000,
        0b11111,
        0b10101,
        0b10101,
        0b00000,
        0b11111,
        0b10100,
        0b11111,
        0b00000,
    ]

    const field = new Array(width).fill(new Array(height).fill(1));

    const isCellActive = (i, j) => arrayActive[i] & Math.pow(2, j);

    return (
        <div className={classNames("running-line", className)}>
            {field.map((column, indexColumn) =>
                <div className={classNames("running-line__column")} key={indexColumn}>
                    {column.map((elem, indexElem) =>
                        <div
                            key={indexElem + indexColumn}
                            className={classNames(
                                "running-line__rect",
                                {"running-line__rect--active": isCellActive(indexColumn, indexElem)}
                            )}
                        />
                    )}
                </div>
            )}
        </div>
    );
}

export default RunningLine;
