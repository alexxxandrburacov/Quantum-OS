import React, {useEffect} from 'react';
import classNames from "classnames";
import AppInList from "../ui/AppInList";
import animations from "../animations";


const AppsList = React.forwardRef(({ className, content, isOpen, setOpen }, ref) => {

    useEffect(() => {
        const appsButton = document.querySelector(`.apps-icons`)

        const checkClickedOutside = e => {
            if (
                ref.current &&
                isOpen &&
                (!(e.path.includes(ref.current)) &&
                !(e.path.includes(appsButton)))
            ) {
                animations.appsListAnim(ref.current, true, setOpen(false))
                animations.appsButtonAnim()
            }
        }

        window.addEventListener("mousedown", checkClickedOutside)

        return () => {
            window.removeEventListener("mousedown", checkClickedOutside)
        }
    }, [isOpen])

    return <div
        ref={ref}
        className={classNames(
            "apps-list",
            className,
        )}
    >
        <span className={classNames("apps-list__header")}>Apps</span>
        <div className={classNames("apps-list__apps-container")}>
            {content.map((app, index) =>
                <AppInList
                    listRef={ref}
                    appData={app}
                    className={classNames("apps-list__app")}
                    key={index}
                />
            )}
        </div>
    </div>
})

export default AppsList
