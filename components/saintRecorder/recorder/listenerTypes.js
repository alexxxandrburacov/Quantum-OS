const listenerTypes = [
    {
        type: 'click',
    },
    {
        type: 'scroll',
        targetProperties: ['scrollTop'],
        capture: true,
    },
    {
        type: 'mouseup',
    },
    {
        type: 'mousedown',
        properties: ['offsetX', 'offsetY'],
    },
    {
        type: 'mousemove',
        properties: ['offsetX', 'offsetY', 'movementX', 'movementY', 'clientX', 'clientY'],
    },
];

export default listenerTypes;
