export const mapProperties = {
    path: 'q',
    type: 'w',
    clientX: 'e',
    clientY: 'r',
    offsetX: 't',
    offsetY: 'y',
    movementX: 'u',
    movementY: 'i',
    scrollTop: 'o',
    frameTime: 'p',
}

export const mapEvents = {
    click: 'q',
    scroll: 'w',
    mouseup: 'e',
    mousedown: 'r',
    mousemove: 't',
}
