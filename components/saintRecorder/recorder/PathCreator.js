class PathCreator {

    makePath(element, DOMPath) {
        const topElement = window.document.body;
        const path = [];

        let currentElement = element;
        let index = 0;
        while (true) {
            if (!currentElement.parentNode) {
                currentElement = DOMPath[index + 1];
            }

            if (currentElement === topElement || element === topElement || !element) return path;
            const parent = currentElement.parentNode;

            path.push(Array.from(parent?.children).indexOf(currentElement));

            currentElement = parent;

            index++;
        }
    }

    getElementFromPath = (path) => {
        let layerElement = document.body;

        path.reverse().map((layer) => {
            layerElement = layerElement?.children[layer];
        })

        return layerElement;
    }
}

export default PathCreator;
