export const switchKeyValue = (obj) => {
    const newObj = {};

    Object.entries(obj).map(([ key, value ]) => {
        newObj[value] = key;
    });

    return newObj;
}
