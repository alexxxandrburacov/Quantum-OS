import listenerTypes from './listenerTypes';
import PathCreator from "./PathCreator";
import {mapEvents, mapProperties} from "./mapProperties";
import Player from "./Player";

export default class Recorder {

    init() {
        this.pathCreator = new PathCreator();
        this.player = new Player();

        this.listeners = {};
        this.recordData = [];
        this.startTime = Date.now();

        listenerTypes.map(({ type, properties, targetProperties, capture }) => {
            window.addEventListener(type, this.#createListener(type, properties, targetProperties, capture), capture)
        })

        setTimeout(() => {
            console.log(JSON.stringify(this.recordData))
        }, 10000)
    };

    detach() {
        Object.entries(this.listeners).forEach(([type, { listener, capture }]) => {
            window.removeEventListener(type, listener, capture);
        });
    }

    #createListener(type, properties, targetProperties, capture) {
        const listener = (event) => {
            if (!event.isTrusted) {
                this.detach();
                return;
            }

            const tick = {
                [mapProperties.path]: this.pathCreator.makePath(event.target, event.path),
                [mapProperties.type]: mapEvents[event.type],
                [mapProperties.frameTime]: Date.now() - this.startTime,
            }

            properties?.map((property) => tick[mapProperties[property]] = event[property]);
            targetProperties?.map((property) => tick[mapProperties[property]] = event.target[property]);

            this.recordData.push(tick);
        }

        this.listeners[type] = {
            listener,
            capture,
        };

        return listener;
    };
}
