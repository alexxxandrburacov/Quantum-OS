import {switchKeyValue} from "./utils";
import {mapEvents, mapProperties} from "./mapProperties";
import PathCreator from "./PathCreator";

const extractEvents = switchKeyValue(mapEvents);

class Player {

    constructor() {
        this.pathCreator = new PathCreator();

        window.addEventListener('message', (message) => this.#onMessage(message))
    }

    #onMessage(message) {
        if (message.data.isEvent) {
            const { tick } = message.data;

            const event = new Event(extractEvents[tick[mapProperties.type]], {
                bubbles: true,
            })

            event.clientY = tick[mapProperties.clientY];
            event.clientX = tick[mapProperties.clientX];
            event.offsetY = tick[mapProperties.offsetY];
            event.offsetX = tick[mapProperties.offsetX];
            event.movementX = tick[mapProperties.movementX];
            event.movementY = tick[mapProperties.movementY];

            const element = this.pathCreator.getElementFromPath(tick[mapProperties.path]);
            if (element) {
                element.scrollTop = tick[mapProperties.scrollTop];
                element.dispatchEvent(event);
            }
        }
    }
}

export default Player;
