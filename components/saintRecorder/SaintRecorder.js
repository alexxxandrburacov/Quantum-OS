import React, {useEffect} from 'react';
import Recorder from './recorder/Recorder'

const SaintRecorder = ({ children }) => {

    useEffect(() => {
        const recorder = new Recorder();

        //recorder.init();

        return () => {
            recorder.detach();
        }
    }, [])

    return children;
}

export default SaintRecorder;
