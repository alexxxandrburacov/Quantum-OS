import React, { useContext } from 'react';
import classNames from "classnames";

import Rectangle from "../assets/icons/rectangle.svg"
import Ellipse from "../assets/icons/ellipse.svg"
import Rhombus from "../assets/icons/rhombus.svg"
import Arrow from "../assets/icons/arrow.svg"
import CoreContext from "../contexts/context";
import { em } from "../tools/helpers";
import windowThemes from "../config/constants/windowThemes";


function DragBar({className, app, isFullScreen, setFullScreen}) {
    const {closeWindow, minimizeWindow} = useContext(CoreContext)

    return (
        <div className={classNames(
            "drag-bar",
            `drag-bar--${app.theme || windowThemes.LIGHT}`,
            className,
        )}>
            <span className={classNames("drag-bar__app-name")}>{app.name}</span>
            <div className={classNames("drag-bar__buttons-container")}>
                {app.resize && (isFullScreen ?
                    <Ellipse
                        className={classNames(
                            "drag-bar__button",
                            "drag-bar__button--display"
                        )}
                        onClick={() => setFullScreen(false)}
                    />
                    :
                    <Rectangle
                        className={classNames(
                            "drag-bar__button",
                            "drag-bar__button--display"
                        )}
                        onClick={() => setFullScreen(true)}
                    />)
                }

                <Arrow
                    key={app.name}
                    className={classNames(
                        "drag-bar__button",
                        "drag-bar__button--hide"
                    )}
                    onClick={() => minimizeWindow(app.slug, true)}
                />
                <Rhombus
                    onClick={() => closeWindow(app.slug)}
                    className={classNames(
                        "drag-bar__button",
                        "drag-bar__button--close"
                    )}
                />
            </div>
        </div>
    );
}

export default DragBar;
