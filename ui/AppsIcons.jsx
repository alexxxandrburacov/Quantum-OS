import React, {useEffect} from 'react';
import classNames from "classnames";
import Ellipse from "../assets/icons/ellipse.svg"
import Rhombus from "../assets/icons/rhombus.svg"
import Rectangle from "../assets/icons/rectangle.svg"
import {gsap} from "gsap";
import animations from "../animations";

function AppsIcons({ className, onClick, isAppsOpen }) {

    return (
        <div
            className={classNames(
                "apps-icons",
                className,
            )}
            onClick={() => {
                onClick()
                animations.appsButtonAnim(isAppsOpen)
            }}
        >
            <div className={classNames("apps-icons__icons-container", "apps-icons__icons-container--upper")}>
                <Ellipse className={classNames(
                    "apps-icons__icon",
                    "apps-icons__icon--ellipse"
                )}/>
                <Rectangle className={classNames(
                    "apps-icons__icon",
                    "apps-icons__icon--rectangle"
                )}/>
            </div>
            <div className={classNames("apps-icons__icons-container", "apps-icons__icons-container--lower")}>
                <Rhombus className={classNames(
                    "apps-icons__icon",
                    "apps-icons__icon--rhombus"
                )}/>
                <Ellipse className={classNames(
                    "apps-icons__icon",
                    "apps-icons__icon--ellipse"
                )}/>
            </div>
        </div>
    );
}

export default AppsIcons;
