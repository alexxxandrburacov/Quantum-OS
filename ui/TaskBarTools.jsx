import React, {createRef, useContext, useRef} from 'react';
import classNames from "classnames";
import Sound from "../assets/icons/sound.svg"
import Arrow from "../assets/icons/arrow.svg"
import CoreContext from "../contexts/context";
import animations from "../animations";
import { animationsConfig } from "../config/animations";
import { gsap } from "gsap"

function TaskBarTools({ className, notificationRef }) {
    const { setNotificationsOpen, isNotificationsOpen } = useContext(CoreContext)
    const arrowRef = useRef(null)

    return (
        <div className={classNames(
            "task-bar-tools",
            className
        )}>
            {/*<Sound className={classNames(*/}
            {/*    "task-bar-tools__tool",*/}
            {/*    "task-bar-tools__tool--sound"*/}
            {/*)}*/}
            {/*/>*/}
            <div className={classNames("task-bar-tools__notifications-container")}>
                <span className={classNames("task-bar-tools__notifications-amount")}>1</span>

                <div className={classNames("task-bar-tools__arrow-container")} ref={arrowRef}>
                    <Arrow
                        className={classNames(
                            "task-bar-tools__tool",
                            "task-bar-tools__tool--arrow",
                        )}
                        onClick={() => {
                            animations.rotateAnim(arrowRef.current, "180deg", isNotificationsOpen)
                            animations.notificationsAnim(
                                notificationRef.current,
                                isNotificationsOpen,
                                () => setNotificationsOpen(!isNotificationsOpen)
                            )
                        }}
                    />
                </div>

            </div>


        </div>
    );
}

export default TaskBarTools;
