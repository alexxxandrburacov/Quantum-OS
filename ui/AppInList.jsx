import React, {useContext, useEffect} from 'react';
import classNames from "classnames";
import Dots from "../assets/icons/dots.svg"
import CoreContext from "../contexts/context";
import animations from "../animations";
import {animationsConfig} from "../config/animations";

function AppInList({ className, appData, listRef }) {
    const { openWindow, setAppsOpen, runningApps } = useContext(CoreContext)

    const listOnComplete = () => {
        setAppsOpen(false)
    }

    return (
        <div
            className={classNames("app-in-list", className)}
            onClick={() => {
                animations.appsListTransferAnim(listRef.current, () => listOnComplete())
                animations.appsButtonAnim()

                setTimeout(
                    () => {
                        openWindow(appData?.name)
                        animations.openWindowAnim(`.${appData?.slug.toLowerCase()}-app`)
                    }, animationsConfig.fastAnimation / 2
                )

            }}
        >
            <div className={classNames("app-in-list__container")}>
                <div className={classNames("app-in-list__icon")}>{appData?.icon}</div>
                <div className={classNames("app-in-list__content")}>
                    <span className={classNames("app-in-list__name")}>{appData?.name}</span>
                    <span className={classNames("app-in-list__description")}>{appData?.description}</span>
                </div>
            </div>
        </div>
    );
}

export default AppInList;
