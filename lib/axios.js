import axios from 'axios';

export const imagesAxios = axios.create({
    baseURL: process.env.NEXT_PUBLIC_PATH,
});
