import React, {useEffect} from "react";
import '../styles/app.scss'

function App({ Component }) {

  return (
      <Component />
  )
}

export default App
