import React from "react";
import Main from "../components/screens/Main";
import Core from "../components/core/Core";
import SaintRecorder from "../components/saintRecorder";

export default function Home() {
    return (
        <SaintRecorder>
            <Core.Provider>
                <Main />
            </Core.Provider>
        </SaintRecorder>
    )
}
