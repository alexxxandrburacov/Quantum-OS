import React from "react";
import classNames from "classnames";
import SkillCard from "../components/cards/SkillCard";

function Comp() {
    const content = {
        header: "React",
        description: "I can make a website, simple or more complex, for example, like this. " +
            "As far as my experience goes, I try to keep the code clean and convenient, readable architecture."
    }

    return (
        <div className={classNames("comp")}>
            <SkillCard className={classNames("comp__skill")} content={content}/>
        </div>
    )
}

export default Comp
