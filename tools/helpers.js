import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

export const em = (pixels, fontSize = 16) => {
    return pixels / fontSize + 'em'
};

export const getTranslate = (runningAppsLength, index) => {
    if (runningAppsLength % 2 === 0) {
        return (48 / 2) - ((runningAppsLength / 2) * 48) + (index * 48)
    } else {
        return -(Math.floor(runningAppsLength / 2) * 48) + (index * 48)
    }
}
