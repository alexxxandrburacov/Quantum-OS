const path = require('path');

module.exports = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/,
      issuer:  /\.(js|ts|jsx|tsx)x?$/,
      use: [{
        loader:'@svgr/webpack',
        options: {
          svgoConfig: {
            plugins: [{
              cleanupIDs: {
                "prefix": `svg_${Math.random().toString(36).substr(2, 9)}`
              }
            }]
          }
        }
      }]
    });
    return config;
  },
}
