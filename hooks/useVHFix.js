import { useEffect } from "react";

const useVHFix = () => {
    useEffect(() => {
        const resize = () => {
            const vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
        }
        resize();
        window.addEventListener('resize', resize);

        return () => {
            window.removeEventListener('resize', resize);
        }
    }, [])

    return null;
}

export default useVHFix;
