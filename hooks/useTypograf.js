import Typograf from 'typograf';

const useTypograf = () => {
    const tp = new Typograf({
        locale: ['en-US'],
    });

    return (text, hasWrapper = true) => {
        if (!text) return null;
        return hasWrapper ?
            <span dangerouslySetInnerHTML={{__html: tp.execute(text) }} /> :
            tp.execute(text);
    }
}

export default useTypograf;
