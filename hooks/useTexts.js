import useTypograf from "./useTypograf";
import {useSelector} from "react-redux";
import {dictionary} from "../redux/selectors/main";

const useTexts = () => {
    const tp = useTypograf();
    const dictionaryData = useSelector(dictionary)
    return key => {
        let text = key;

        if (key in dictionaryData) {
            text = dictionaryData[key];
        } else {
            console.warn(`No text for key: ${key}`)
        }

        return tp(text);
    }
}

export default useTexts;
