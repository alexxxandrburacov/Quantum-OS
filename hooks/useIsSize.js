import { useState, useEffect } from 'react';

const useIsSize = size => {

    const [captured, setCaptured] = useState(false);

    const resize = () => {
        setCaptured(window.innerWidth < size);
    }

    useEffect(() => {
        window.addEventListener("resize", resize);
        resize();

        return () => {
            window.removeEventListener("resize", resize);
        };
    }, []);
    return captured;
}

export default useIsSize;
