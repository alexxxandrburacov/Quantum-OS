import React from 'react';
import classNames from "classnames";

function ContentContainer({ className, children, isSmall = false, onClick = () => {} }) {
    return (
        <div onClick={onClick} className={classNames(
            "content-container",
            className,
            {
                "content-container--small": isSmall,
                "content-container--big": !isSmall,
            }
        )}>
            {children}
        </div>
    );
}

export default ContentContainer;
