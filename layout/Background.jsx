import React from 'react';
import classNames from "classnames";
import decor1 from '/assets/decor/decor-blue.png';
import decor2 from '/assets/decor/decor-red.png';

const Background = ({ className }) => {
    return (
        <div className={classNames(className, 'background')}>
            <div className={classNames('background__content')}>
                <img alt="" src={decor1.src} className={classNames('background__decor', 'background__decor--blue')} />
                <img alt="" src={decor2.src} className={classNames('background__decor', 'background__decor--red')} />
            </div>
        </div>
    )
}

export default Background;
