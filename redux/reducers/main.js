import {SET_CONFIG, SET_DEVELOPERS, SET_DICTIONARY, SET_NEWS} from '../constants';
import {HYDRATE} from "next-redux-wrapper";

const defaultState = {
    news: [],
    dictionary: [],
    config: [],
    developers: [],
}

const mainReducer = (state = defaultState, action) => {
    const {type, payload} = action;
    switch (type) {
        case SET_NEWS:
            return {
                ...state,
                news: payload,
            };
        case SET_DICTIONARY:
            return {
                ...state,
                dictionary: payload,
            };
        case SET_CONFIG:
            return {
                ...state,
                config: payload,
            };
        case SET_DEVELOPERS:
            return {
                ...state,
                developers: payload,
            };
        case HYDRATE:
            return {...action.payload.main};
        default:
            return {...state};
    }
};

export default mainReducer;
