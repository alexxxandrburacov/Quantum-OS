import axios from '/lib/axios';
import {SET_CONFIG, SET_DEVELOPERS, SET_DICTIONARY, SET_NEWS} from "../constants";

export const setNews = (payload) => ({
    type: SET_NEWS,
    payload,
})

export const getNews = () => dispatch => axios.get(
    `get-news/`,
).then(response => {
    dispatch(setNews(response.data.list));
})

export const setDictionary = (payload) => ({
    type: SET_DICTIONARY,
    payload,
})

export const getDictionary = () => dispatch => axios.get(
    `get-dictionary/`,
).then(response => {
    dispatch(setDictionary(response.data.list));
})

export const getLecturers = () => axios.get(
    `get-lecturers/`,
)

export const getSessionCards = () => axios.get(
    `get-session-cards/`,
)

export const getPricesCards = () => axios.get(
    `get-prices-cards/`,
)

export const getPhotos = () => axios.get(
    `get-photos/`,
)

export const setDevelopers = (payload) => ({
    type: SET_DEVELOPERS,
    payload,
})

export const getDevelopers = () => dispatch => axios.get(
    `get-devs/`,
).then(response => {
    dispatch(setDevelopers(response.data.list));
})

export const getArticle = (slug) => axios.get(
    `get-article/${slug}/`,
)

export const setConfig = (payload) => ({
    type: SET_CONFIG,
    payload,
})

export const getConfig = () => dispatch => axios.get(
    `get-config/`,
).then(response => {
    dispatch(setConfig(response.data.list));
})



