export const news = (state) => state.main.news;
export const dictionary = (state) => state.main.dictionary;
export const config = (state) => state.main.config;
export const developers = (state) => state.main.developers;
