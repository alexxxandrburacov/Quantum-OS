export const SET_NEWS = 'SET_NEWS';
export const SET_DICTIONARY = 'SET_DICTIONARY';
export const SET_CONFIG = 'SET_CONFIG';
export const SET_DEVELOPERS = 'SET_DEVELOPERS';
