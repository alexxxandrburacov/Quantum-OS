import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import { createWrapper } from "next-redux-wrapper";
import thunk from 'redux-thunk';

const reduxStore = () =>  createStore(rootReducer, applyMiddleware(thunk))

export const wrapper = createWrapper(reduxStore);

