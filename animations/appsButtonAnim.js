import {gsap} from "gsap";

function appsButtonAnim(runBackwards = true) {
    gsap.fromTo(".apps-icons__icons-container--upper",
        {
            top: 0,
            left: 0,
        },
        {
            duration: 0.25,
            top: "9px",
            left: 0,
            runBackwards: runBackwards,
            ease: "out",
        })
    gsap.fromTo(".apps-icons__icons-container--lower",
        {
            top: "15px",
            left: 0,
        },
        {
            top: "7px",
            left: "33px",
            duration: 0.25,
            runBackwards: runBackwards,
            ease: "out",
        })
}

export default appsButtonAnim;
