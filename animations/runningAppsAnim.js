import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";
import {getTranslate} from "../tools/helpers";

function runningAppsAnim(runningApps) {
    runningApps.forEach((app, index) => {
        gsap.to(
            `.${app.slug.toLowerCase()}-icon`,
            {
                duration: animationsConfig.fastAnimation,
                translateX: getTranslate(runningApps.length, index)
            }
        )
    })

}

export default runningAppsAnim
