import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

function appIconAnim(target, runBackwards, onComplete = () => {}) {
    gsap.fromTo(
        target,
        {
            autoAlpha: 0,
        },
        {
            duration: animationsConfig.fastAnimation,
            transformOrigin: "right center",
            autoAlpha: 1,
            runBackwards,
            onComplete,
            ease: "out",
        }
    );
}

export default appIconAnim
