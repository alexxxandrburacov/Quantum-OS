import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

export function closeWindowAnim(target, onComplete) {
    gsap.to(target, {
        duration: animationsConfig.fastAnimation,
        scale: 1.2,
        opacity: 0,
        transformOrigin: "center",
        ease: "out",
        onComplete: onComplete,
    })
}

export function openWindowAnim(target) {
    gsap.fromTo(
        target,
        {
            autoAlpha: 0,
            scale: 0.8,
        },
        {
            duration: animationsConfig.fastAnimation,
            transformOrigin: "left bottom",
            autoAlpha: 1,
            scale: 1,
            ease: "in",
        }
    );
}

