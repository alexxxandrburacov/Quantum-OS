import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

function appsListTransferAnim(target, onComplete) {
    gsap.fromTo(
        target,
        {
            scale: 1,
            autoAlpha: 1,
        },
        {
            duration: animationsConfig.fastAnimation,
            scale: 1.95,
            autoAlpha: 0,
            ease: "out",
            onComplete: onComplete,
        }
    );
    target.style.scale = 0.1
}

export default appsListTransferAnim
