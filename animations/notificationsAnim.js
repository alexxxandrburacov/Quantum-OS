import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

function notificationsAnim(target, runBackwards, onComplete) {
    let ease

    if (runBackwards) {
        ease = "out"
    } else {
        ease = "in"
    }

    gsap.fromTo(
        target,
        {
            scale: 0.1,
            autoAlpha: 0,
        },
        {
            duration: animationsConfig.fastAnimation,
            transformOrigin: "right bottom",
            scale: 1,
            autoAlpha: 1,
            ease: ease,
            runBackwards: runBackwards,
            onComplete: onComplete,
        }
    );
}

export default notificationsAnim
