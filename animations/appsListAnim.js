import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

function appsListAnim(target, runBackwards, onComplete) {
    let ease

    if (runBackwards) {
        ease = "out"
    } else {
        ease = "in"
    }

    gsap.fromTo(
        target,
        {
            autoAlpha: 0,
            scale: 0.1,
        },
        {
            autoAlpha: 1,
            duration: animationsConfig.fastAnimation,
            transformOrigin: "left bottom",
            scale: 1,
            runBackwards: runBackwards,
            ease: ease,
            onComplete: onComplete,
        }
    );
}

export default appsListAnim
