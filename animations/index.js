import {gsap} from "gsap";
import {CustomEase} from "gsap/dist/CustomEase";
import appsButtonAnim from "./appsButtonAnim";
import appsListAnim from "./appsListAnim";
import minimizeWindow from "./minimizeWindow";
import { closeWindowAnim, openWindowAnim } from "./windowAnim";
import runningAppsAnim from "./runningAppsAnim";
import appsListTransferAnim from "./appsListTransferAnim";
import appIconAnim from "./appIconAnim";
import notificationsAnim from "./notificationsAnim";
import rotateAnim from "./rotateAnim";

gsap.registerPlugin(CustomEase);
CustomEase.create("out", "0.89, -0.34, 0.57, 0.99")
CustomEase.create("in", "0.53, 0.01, 0.19, 1.35")

const animations = {
    appsButtonAnim,
    appsListAnim,
    appsListTransferAnim,
    minimizeWindow,
    closeWindowAnim,
    openWindowAnim,
    runningAppsAnim,
    appIconAnim,
    notificationsAnim,
    rotateAnim
}

export default animations








