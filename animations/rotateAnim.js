import {gsap} from "gsap";
import {animationsConfig} from "../config/animations";

function rotateAnim(target, deg, runBackwards) {
    gsap.fromTo(target,
        {
            rotate: "0"
        },
        {
            duration: animationsConfig.fastAnimation,
            rotate: deg,
            runBackwards: runBackwards,
            transformOrigin: "center",
        }
    )
}

export default rotateAnim
