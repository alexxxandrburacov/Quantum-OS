import {gsap} from "gsap";
import {em} from "../tools/helpers";
import {animationsConfig} from "../config/animations";

function minimizeWindow(target, runBackwards, onComplete) {
    const rect = target.getBoundingClientRect()
    let transformOrigin = -(rect.x + - (window.innerWidth / 2)) + "px bottom"
    let ease

    if (runBackwards) {
        ease = "out"
    } else {
        ease = "in"
    }

    gsap.fromTo(
        target,
        {
            autoAlpha: 0,
            scale: 0.1,
            translateY: em((window.innerHeight - 80) - (rect.y + rect.height)),
        },
        {
            autoAlpha: 1,
            duration: animationsConfig.fastAnimation,
            scale: 1,
            transformOrigin: transformOrigin,
            runBackwards: runBackwards,
            translateY: 0,
            ease: ease,
            onComplete: onComplete
        }
    );
}

export default minimizeWindow
